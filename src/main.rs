use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::io::BufWriter;
use std::io::BufReader;
use std::fs::OpenOptions;
use std::io;
use std::io::BufRead;

fn main() {
    let file_name = "./db.txt";
    let quit = "quit";
    let set_com = "set";
    let get_com = "get";
    let run_program = true;

    let mut command = String::new();

    println!("Type one of:");
    println!("{}", set_com);
    println!("{}", get_com);
    println!("{}(q)", quit);

    while run_program {

        io::stdin().read_line(&mut command);

        println!("You typed: {}", command.trim());

        match command.trim() as &str {
            "set" => {
                set_case(file_name);
            }

            "get" =>{
                get_case(file_name);
            }

            "quit" | "q" => {
              break;
            }

            _ =>{
                println!("Unknown command");
            }
        }

        command.clear();
    }

    println!("Exit");
}

fn get_db_file(file_name : String){

    let mut f = File::open(file_name).expect("file not found");

    let mut content = String::new();

    f.read_to_string(&mut content).expect("something went wrong reading the file");

    //println!("file content: " + content);
}

fn write_to_db_file(file_name : &str, text : String) -> std::io::Result<()>{

    let f = OpenOptions::new()
        .create(true)
        .append(true)
        .open(file_name) ?;

    let mut writer = BufWriter::new(f);

    let text_copy = text.trim().to_string();
    let new_line = "\n";
    let text_to_write = text_copy + new_line;
    let text_ba = (text_to_write).as_bytes();
    writer.write_all(text_ba) ? ;

    Ok(())
}


fn validate_key_value(input : String) -> bool {

    let splited_input = input.trim().split(":");

    let vec = splited_input.collect::<Vec<&str>>();

    return vec.len() == 2;
}

fn set_case(file_name : &str){
    println!("Type the key:value");

    let mut command = String::new();
    io::stdin().read_line(&mut command).expect("some error");

    if !validate_key_value(command.clone()){
        println!("Incorrect key:value statement");
        return;
    }

    println!("Writing to db");

    let val_to_write = command.clone();
    write_to_db_file(file_name, val_to_write);

    println!("Wrote to db");
}

fn get_case(file_name : &str) -> std::io::Result<()>{
    println!("Type the key");

    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("some error");

    let f = OpenOptions::new()
        .read(true)
        .open(file_name) ?;

    let reader = BufReader::new(f);

    let inpt_trim = input.trim();

    let search_result = search_key(inpt_trim, reader);

    if search_result.success{
        println!("Found value by key is: {}", search_result.found_val);
    }

    Ok(())
}

struct KeySearchResult{
    found_val: String,
    success : bool
}

fn search_key(search_key : &str, reader : BufReader<File>) -> KeySearchResult{
    let mut found_value = String::new();
    let mut is_found = false;
    for line in reader.lines() {

        let line_str = line.expect("error");
        if  line_str.trim().is_empty(){
            continue;
        }

        let value = line_str.split(":");
        let vec = value.collect::<Vec<&str>>();

        let key = vec[0].trim();
        if search_key == key {
            is_found = true;
            found_value = String::from(vec[1]);
        }
    }

    return KeySearchResult{ found_val: found_value, success : is_found };
}


